FROM mcr.microsoft.com/dotnet/aspnet:5.0

#### Install gcsfuse
RUN apt-get update && \
    apt-get -y install \
      curl \
      gnupg 

RUN echo "deb http://packages.cloud.google.com/apt gcsfuse-stretch main" > /etc/apt/sources.list.d/gcsfuse.list && \
     curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

RUN apt-get update && \
     apt-get -y install gcsfuse jq && \
     rm -rf /var/lib/apt/lists/* && \
     mkdir -p /var/run/sshd && \
     rm -f /etc/ssh/ssh_host_*key*

#RUN ["apt-get", "update"]
#RUN ["apt-get", "-y", "install", "postgresql-client-common"]
#RUN ["apt-get", "-y", "install", "postgresql-client"]



RUN printf "\nuser_allow_other\n" >> /etc/fuse.conf

